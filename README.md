Loan Management system  is a  Python Django  system which enables customers to register then wait for approval for them to apply for loan.

The system has three sections the Loan officers section which includes the approval of customers for loan and messages  for loan status.

The  administrator  section which entails all users management , system statistics, loan calculations  and messages .

Customers sections after registrations then approval includes ; customer loan application , appropriate messages for loan status , details for loan disbursement.

The system uses MySQL database :



Logins :
Admin

user: admin@gmail.com

passwd: password

Loan officers

user: mary@gmail.com
passwd: pass123


Customer:

user: edwin@gmail.com
passwd: abc123


Requirements


Django==1.9 # pip install django


django-crispy-forms #pip  install django-crispy-forms


django-celery #pip install  django-celery

rabbitmq-server_3.6.5-1_all.deb # dpkg  -i rabbitmq-server_3.6.5-1_all.deb (download) or apt-get install rabbitmq-server